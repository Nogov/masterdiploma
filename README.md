# README #

The points of interest are UpperBoundEstimator.cs, ParallelEstimator.cs, and CLEFIA.cs.
This application estimates an upper bound of differential probability for CLEFIA-like ciphers against differential cryptanalysis.

A big part of code was written to provide a balance between readability and performance. 
For instance, I've provided a syntax like 
```
#!c#

UB[r](a, b)
```
 as the real formula looks like.

This app works a lot with the file system. Good for me, I have SSD, so it works much faster. But also, it uses LZ4 compression algorithm for IO operations which gives some performance optimization.

The calculations took long period of time, so I had to calculate in %cpu_cores% different threads.