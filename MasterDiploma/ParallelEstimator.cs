﻿using MasterDiplom.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace MasterDiplom
{
	public abstract class ParallelEstimator : UpperBoundEstimator
	{
		private static readonly string RootPath = ConfigurationManager.AppSettings.Get("RootPath");
		private static int PrcCount = Environment.ProcessorCount;

		private int _initialRound = -1;
		private UpperBoundFunc[] UB;

		/// <summary>
		/// A pool of previous upper bound matrix rows.
		/// </summary>
		/// <remarks>Used to encapsulate working with file system for children classes.</remarks>
		protected static readonly Dictionary<Vector, double[]> RowPool
			= new Dictionary<Vector, double[]>(PrcCount << 1);

		/// <summary>
		/// Upper bounds limits of a previous round which cannot be exceeded.
		/// </summary>
		protected double[] Bounds;

		protected ParallelEstimator(int tplSize)
			: base(tplSize)
		{
			Bounds = new double[_ubSize];
		}

		private string GetRoundFileName(int round, int part)
			=> $@"{RootPath}\ub_r{round}_part_{part}";

		private string GetBoundsFileName(int round)
			=> $@"{RootPath}\bounds_r{round}";

		private double EstimateInParallel(int round, int left, int right)
		{
			var result = 0d;
			var tasks = new Task[PrcCount];

			for (int n = 0; n < PrcCount; n++)
			{
				var thread = n;
				var partSize = (right - left) / PrcCount;
				var from = left + thread * partSize;
				var to = left + (thread + 1) * partSize;
				tasks[n] = ProcessAsync(thread, round, from, to)
					.ContinueWith(t => result = Math.Max(t.Result, result));
			}
			Task.WaitAll(tasks);
			WriteBounds(round);

			return result;
		}

		private void WriteBounds(int round)
		{
			var fileName = GetBoundsFileName(round);
			using (var boundsWriter = new CompressionWriter(fileName))
				boundsWriter.Write(Bounds);
		}

		private Vector CreateVector(UInt value)
		{
			return new Vector(
				(value & 0xf000) >> 12,
				(value & 0x0f00) >> 8,
				(value & 0x00f0) >> 4,
				(value & 0x000f) >> 0
			);
		}

		private async Task<double> ProcessAsync(int part, int round, int from, int to)
		{
			Func<double> flow = CreateFlowFunc(part, round, from, to);
			return await Task.Run(flow);
		}

		private Func<double> CreateFlowFunc(int thread, int r, int from, int to)
		{
			return () =>
			{
				var result = 0d;
				var isMinRound = r == _minRound;

				var writeTo = GetRoundFileName(r, thread);
				var readFrom = GetRoundFileName(r - 1, thread);

				using (var writer = new CompressionWriter(writeTo))
				{
					var reader = isMinRound
						? default(CompressionReader)
						: new CompressionReader(readFrom);
					using (reader)
					{
						for (var tpl_a = from; tpl_a < to; tpl_a++)
						{
							var row = new double[_ubSize];
							var a = CreateVector(tpl_a);
							if (!isMinRound)
								RowPool.Add(a, reader.Read<double>(_ubSize, sizeof(double)));
							for (var tpl_b = 1; tpl_b < _ubSize; tpl_b++)
							{
								var b = CreateVector(tpl_b);
								var upperBound = UB[r](a, b);

								if (upperBound > 0 && (Bounds[tpl_b] == 0 || Bounds[tpl_b] > upperBound))
									Bounds[tpl_b] = upperBound;
								if (upperBound > result)
									result = upperBound;

								row[tpl_b] = upperBound;
							}
							if (!isMinRound)
								RowPool.Remove(a);
							writer.Write(row);
						}
					}
				}
				return result;
			};
		}

		/// <summary>
		/// Estimates upper bound of differential probability for given amount of rounds
		/// for a chunk of values from <paramref name="left"/> to <paramref name="right"/>.
		/// </summary>
		/// <param name="rounds">Rounds amount.</param>
		/// <param name="left">Left chunk bound.</param>
		/// <param name="right">Right chunk bound.</param>
		/// <returns>Probability upper bound.</returns>
		public virtual double EstimateChunk(int rounds, int left, int right)
		{
			if (_initialRound == -1)
				_initialRound = _minRound;
			var result = 0d;
			UB = GetEstimationFunctions(rounds);
			for (var r = _initialRound; r <= rounds; r++)
			{
				result = EstimateInParallel(r, left, right);
				var line = $"UB[{r}](a, b) ~ 2^{Math.Round(Math.Log(result, 2), 2)}";
				Console.WriteLine(line);
			}
			return result;
		}

		/// <summary>
		/// Runs custom estimation of upper bound of differential probability for given amount of rounds
		/// starting from <paramref name="startFrom"/>-th round 
		/// for a chunk of values from <paramref name="left"/> to <paramref name="right"/>.
		/// </summary>
		/// <param name="rounds">Rounds amount.</param>
		/// <param name="startFrom">Round to start from.</param>
		/// <param name="left">Left chunk bound.</param>
		/// <param name="right">Right chunk bound.</param>
		/// <returns>Probability upper bound.</returns>
		public double CustomEstimation(int rounds, int startFrom, int left, int right)
		{
			_initialRound = startFrom;
			if (_initialRound > _minRound)
			{
				var fileName = GetBoundsFileName(startFrom - 1);
				using (var boundsReader = new CompressionReader(fileName))
					Bounds = boundsReader.Read<double>(_ubSize, sizeof(double));
			}
			return EstimateChunk(rounds, left, right);
		}

		/// <summary>
		/// Estimates upper bound of differential probability for given rounds amount.
		/// </summary>
		/// <param name="rounds">Rounds amount.</param>
		/// <returns>Probability upper bound.</returns>
		public override double Estimate(int rounds) 
			=> EstimateChunk(rounds, 0, _ubSize);
	}
}