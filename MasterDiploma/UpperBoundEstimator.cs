﻿using System.Collections.Generic;

namespace MasterDiplom
{
	using Tools;
	using static Tools.Extensions;

	/// <summary>
	/// Abstract upper bound estimator.
	/// </summary>
	public abstract class UpperBoundEstimator
	{
		/// <summary>
		/// Signature definition for upper bound estimation function.
		/// </summary>
		/// <param name="input">Input vector.</param>
		/// <param name="output">Output vector.</param>
		/// <returns>Upper bound.</returns>
		protected delegate double UpperBoundFunc(Vector input, Vector output);

		/// <summary>
		/// Round number to apply primitive estimation function.
		/// </summary>
		protected int _minRound;

		/// <summary>
		/// Template size in bits.
		/// </summary>
		protected int _tplSize;

		/// <summary>
		/// Upper bounds matrix width/height.
		/// </summary>
		protected int _ubSize;

		/// <summary>
		/// Precalculated probabilities.
		/// </summary>
		protected abstract double[][] P { get; }

		/// <summary>
		/// Count of blocks.
		/// </summary>
		protected abstract int BlocksCount { get; }

		private List<UInt>[,] _phi;
		/// <summary>
		/// Set of templates for sum.
		/// </summary>
		/// <remarks>Uses lazy load.</remarks>
		protected List<UInt>[,] Phi
		{
			get
			{
				return _phi ?? (_phi = GetTemplatesSet());
			}
		}

		/// <summary>
		/// Count matrix for linear function templates matches.
		/// </summary>
		protected UInt[,] W { get; } = new UInt[,] {
		#if TWOBIT			
			{ 1, 0, 0, 0 },
			{ 0, 1, 0, 65534 },
			{ 0, 0, 0, 65534 },
			{ 1, 65534, 65534, 2875471592 }
		#else
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255 },
			{ 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0, 255, 0, 255, 255, 64005 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255 },
			{ 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0, 255, 0, 255, 255, 64005 },
			{ 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0, 255, 0, 255, 255, 64005 },
			{ 0, 0, 0, 255, 0, 255, 255, 64005, 0, 255, 255, 64005, 255, 64005, 64005, 16323825 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255 },
			{ 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0, 255, 0, 255, 255, 64005 },
			{ 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0, 255, 0, 255, 255, 64005 },
			{ 0, 0, 0, 255, 0, 255, 255, 64005, 0, 255, 255, 64005, 255, 64005, 64005, 16323825 },
			{ 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0, 255, 0, 255, 255, 64005 },
			{ 0, 0, 0, 255, 0, 255, 255, 64005, 0, 255, 255, 64005, 255, 64005, 64005, 16323825 },
			{ 0, 0, 0, 255, 0, 255, 255, 64005, 0, 255, 255, 64005, 255, 64005, 64005, 16323825 },
			{ 0, 255, 255, 64005, 255, 64005, 64005, 16323825, 255, 64005, 64005, 16323825, 64005, 16323825, 16323825, 4162570275 }
		#endif
		};

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="tplSize">Template size.</param>
		protected UpperBoundEstimator(int tplSize = Default.TPL_SIZE)
		{
			_tplSize = tplSize;
			_ubSize = 1 << (_tplSize * BlocksCount);
		}

		private List<UInt>[,] GetTemplatesSet()
		{
			var size = 1 << _tplSize;
			var phi = new List<UInt>[size, size];
			for (var a = 0u; a < size; a++)
				for (var b = 0u; b < size; b++)
				{
					var tpls = new List<UInt>(16);
					tpls.Add(a ^ b);
					var c = a & b;
					if (c != 0)
						for (int i = 0; i < _tplSize; i++)
						{
							if ((c & (1 << i)) == 0)
								continue;
							var subset = new List<UInt>(16);
							tpls.ForPart(x => subset.Add(x ^ (1 << i)));
							tpls.AddRange(subset);
						}
					phi[a, b] = tpls;
				}
			return phi;
		}

		/// <summary>
		/// Gets the estimation functions preset for given amount of rounds.
		/// </summary>
		/// <param name="rounds">Amount of rounds.</param>
		/// <returns>Estimation functions preset.</returns>
		protected UpperBoundFunc[] GetEstimationFunctions(int rounds)
		{
			var ub = new UpperBoundFunc[rounds + 1];
			ub[_minRound] = Primitive;
			for (int round = _minRound + 1; round <= rounds; round++)
			{
				//Disclosure
				var r = round;
				ub[r] = Iterative;
			}
			return ub;
		}

		/// <summary>
		/// Template wrapper.
		/// </summary>
		/// <param name="value">Value.</param>
		/// <returns>Template of value.</returns>
		protected UInt T(UInt value)
		{
			return value.Template(_tplSize);
		}

		/// <summary>
		/// Indicates whether an intermediate template (<paramref name="g"/>)
		/// matches input (<paramref name="a"/>) and output (<paramref name="b"/>) templates.
		/// </summary>
		/// <param name="g">Intermediate template.</param>
		/// <param name="a">Input template.</param>
		/// <param name="b">Output template.</param>
		/// <returns>Match result.</returns>
		protected bool TplMatches(UInt g, UInt a, UInt b)
		{
			for (var epsilon = 0; epsilon < 1 << _tplSize; epsilon++)
				if (W[a, epsilon] > 0u && Phi[b, epsilon].Contains(g))
					return true;
			return false;
		}

		/// <summary>
		/// Estimates an upper bound of a <see cref="_minRound"/> round.
		/// </summary>
		/// <param name="input">Input vector.</param>
		/// <param name="output">Output vector.</param>
		/// <returns>Upper bound.</returns>
		protected abstract double Primitive(Vector input, Vector output);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		protected abstract double Iterative(Vector input, Vector output);

		/// <summary>
		/// Previous upper bound matrix.
		/// </summary>
		protected double[,] _previousUBMatrix;

		/// <summary>
		/// Estimates upper bound of differential probability for given cipher and rounds amount.
		/// </summary>
		/// <param name="rounds">Rounds amount.</param>
		/// <returns>Probability upper bound.</returns>
		public virtual double Estimate(int rounds)
		{
			var result = 0d;
			var UB = GetEstimationFunctions(rounds);
			for (var r = _minRound; r <= rounds; r++)
			{
				var ub = new double[_ubSize, _ubSize];
				for (var a = 0u; a < _ubSize; a++)
					for (var b = 1u; b < _ubSize; b++)
					{
						var prob = UB[r](a, b);
						ub[a, b] = prob;
						if (prob > result)
							result = prob;
					}
				_previousUBMatrix = ub;
			}
			return result;
		}
	}
}