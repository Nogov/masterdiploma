﻿using System;
using System.Text;

namespace MasterDiplom
{
	using Tools;

	public struct Vector
	{
		private readonly UInt[] _vector;
		public int Length { get { return _vector.Length; } }

		public Vector(int length)
		{
			_vector = new UInt[length];
		}

		public Vector(params int[] values)
		{
			_vector = values.AsUInts();
		}

		public Vector(params UInt[] values)
		{
			_vector = values;
		}

		public UInt this[int index]
		{
			get { return _vector[index]; }
			set { _vector[index] = value; }
		}

		public static implicit operator Vector(uint v)
		{
			return new Vector(v, 0);
		}

		public static implicit operator Vector(int v)
		{
			return new Vector(v, 0);
		}

		public static Vector operator ^(Vector a, Vector b)
		{
			var c = new Vector(a._vector);
			for (int i = 0; i < a.Length; i++)
				c[i] ^= b[i];
			return c;
		}

		public override string ToString()
		{
			return ToString(10);
		}

		public string ToString(byte @base)
		{
			var sb = new StringBuilder(_vector.Length);
			for (int i = 0; i < _vector.Length; i++)
				sb.Append(Convert.ToString(_vector[i], @base)).Append("; ");
			return sb.ToString();
		}
	}
}