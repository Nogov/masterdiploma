﻿using System;

namespace MasterDiplom
{
	public struct UInt
	{
		private readonly uint _this;

		/// <summary>
		/// Gets bit value at position <paramref name="n"/>.
		/// Numeration starts from the very 'right' bit with number '1'.
		/// </summary>
		/// <param name="n">Bit position.</param>
		/// <returns>0 or 1.</returns>
		public UInt this[int n]
		{
			get { return (_this >> (n - 1)) & 1; }
		}

		/// <summary>
		/// Hamming's weight.
		/// </summary>
		public int Weight
		{
			get
			{
				var tmp = _this;
				int count = 0;
				while (tmp != 0)
				{
					count++;
					tmp &= (tmp - 1);
				}
				return count;
			}
		}

		/// <summary>
		/// Zero.
		/// </summary>
		public static UInt Zero { get { return 0u; } }

		/// <summary>
		/// Gets corresponding template value.
		/// </summary>
		/// <param name="tplSize">Template size in bits.</param>
		/// <returns>Template value.</returns>
		public UInt Template(int tplSize)
		{
			var mask = 0;
			var maskSize = sizeof(uint) * 8 / tplSize;
			for (var i = 0; i < maskSize; i++)
				mask |= 1 << i;

			var result = 0u;
			for (var i = 0; i < tplSize; i++)
				if ((_this & (mask << maskSize * i)) != 0)
					result |= (1u << i);
			return result;
		}

		public UInt Zip(uint other, int shift = 16)
		{
			return (_this << shift) | other;
		}

		public UInt(uint value)
		{
			_this = value;
		}

		public UInt(int value)
		{
			_this = (uint)value;
		}

		public static implicit operator uint(UInt v)
		{
			return v._this;
		}

		public static implicit operator UInt(uint v)
		{
			return new UInt(v);
		}

		public static implicit operator int(UInt v)
		{
			return (int)(v._this);
		}

		public static implicit operator float(UInt v)
		{
			return v._this;
		}

		public static implicit operator UInt(int v)
		{
			return new UInt(v);
		}

		public override string ToString()
		{
			return _this.ToString();
		}

		public string ToString(byte @base)
		{
			return (@base == 16)
				? _this.ToString("X")
				: Convert.ToString(_this, @base);
		}
	}
}