﻿namespace MasterDiplom
{
	public class EstimationJob : IJobDetails
	{
		public int Rounds { get; set; }
		public int From { get; set; }
		public int To { get; set; }
	}
}
