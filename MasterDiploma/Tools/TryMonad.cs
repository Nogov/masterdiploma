﻿using System;

namespace MasterDiplom.Tools
{
	internal static class TryMonad
	{
		public static TResult Try<TSource, TResult>(this TSource obj, Func<TSource, TResult> func)
		{
			try
			{
				return func(obj);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return default(TResult);
		}
	}
}