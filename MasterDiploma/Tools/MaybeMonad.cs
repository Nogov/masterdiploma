﻿using System;

namespace MasterDiplom.Tools
{
	internal static class MaybeMonad
	{
		public static TResult Do<TSource, TResult>(this TSource obj, Func<TSource, TResult> func)
		{
			if (obj == null)
				return default(TResult);
			else
				return func(obj);
		}
	}
}
