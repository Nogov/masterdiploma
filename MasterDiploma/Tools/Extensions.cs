﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MasterDiplom.Tools
{
	public static class Extensions
	{
		public static void ForPart<T>(this IEnumerable<T> set, Action<T> action, int count = -1)
		{
			var iterations = count != -1 ? count : set.Count();
			for (int i = 0; i < iterations; i++)
				action(set.ElementAt(i));
		}

		public static bool IsNullOrEmpty<T>(this IEnumerable<T> set)
		{
			return set == default(IEnumerable<T>) || !set.Any();
		}

		public static void Write<T>(this IEnumerable<T> src, char delimeter = '\n')
		{
			ForPart(src, item => Console.Write($"{item}{delimeter}"));
		}

		public static T[] As<T>(this byte[] src, int arraySize)
		{
			var result = new T[arraySize];
			Buffer.BlockCopy(src, 0, result, 0, src.Length);
			return result;
		}

		public static int Clamp(this int v, int min, int max)
		{
			return v < min ? min : v > max ? max : v;
		}
	}
}