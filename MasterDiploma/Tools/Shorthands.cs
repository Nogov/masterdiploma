﻿namespace MasterDiplom.Tools
{
	internal static class Shorthands
	{
		public static DisposableWatch Watch { get { return new DisposableWatch(); } }
		public static DisposableWatch WatchWithTitle(string title) { return new DisposableWatch(title); }

		public static float MDP(Default.UIntMap f, int bitSize)
		{
			var p = 0f;
			var size = 1 << bitSize;
			for (int a = 1; a < size; a++)
				for (int b = 0; b < size; b++)
				{
					var times = 0f;
					for (int x = 0; x < size; x++)
						if ((f(x ^ a) ^ f(x)) == b)
							times++;
					var tmp = times / size;
					p = tmp > p ? tmp : p;
				}
			return p;
		}

		public static float MDP(UInt[] S)
		{
			return MDP(x => S[x], 8);
		}
	}
}