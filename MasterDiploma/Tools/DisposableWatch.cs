﻿using System;
using System.Diagnostics;

namespace MasterDiplom.Tools
{
	public class DisposableWatch : IDisposable
	{
		protected readonly Stopwatch _stopwatch = new Stopwatch();
		protected readonly string _title = "Timer";

		public DisposableWatch()
		{
			_stopwatch.Start();
		}

		public DisposableWatch(string title)
		{
			_title = title;
			_stopwatch.Start();
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			_stopwatch.Stop();
			Console.WriteLine($"{_title}:\t{_stopwatch.Elapsed}");
		}
	}
}