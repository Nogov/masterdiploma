﻿using LZ4;
using System;
using System.IO;

namespace MasterDiplom.Tools
{
	public class CompressionWriter : IDisposable
	{
		private readonly LZ4Stream _stream;
		public CompressionWriter(string path)
		{
			var fileStream = File.Create(path);
			_stream = new LZ4Stream(fileStream, LZ4StreamMode.Compress);
		}

		public void Write(float[] data)
		{
			var buffer = data.AsBytes();
			_stream.Write(buffer, 0, buffer.Length * sizeof(float));
		}

		public void Write(double[] data)
		{
			var buffer = data.AsBytes();
			_stream.Write(buffer, 0, buffer.Length * sizeof(double));
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
				_stream.Dispose();
		}
	}
}