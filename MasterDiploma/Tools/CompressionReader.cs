﻿using LZ4;
using System;
using System.IO;

namespace MasterDiplom.Tools
{
	public class CompressionReader : IDisposable
	{
		private readonly LZ4Stream _stream;
		public CompressionReader(string path)
		{
			var fileStream = File.OpenRead(path);
			_stream = new LZ4Stream(fileStream, LZ4StreamMode.Decompress);
		}

		public double[] Read(int bytesCount)
		{
			var buffer = new byte[bytesCount];
			_stream.Read(buffer, 0, buffer.Length);
			return buffer.As<double>(bytesCount / sizeof(double));
		}

		public T[] Read<T>(int arraySize, int structSize)
		{
			var buffer = new byte[arraySize * structSize];
			_stream.Read(buffer, 0, buffer.Length);
			return buffer.As<T>(arraySize);
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
				_stream.Dispose();
		}
	}
}
