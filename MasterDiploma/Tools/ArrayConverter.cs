﻿using System.Runtime.InteropServices;

namespace MasterDiplom.Tools
{
	/// <summary>
	/// A tricky utility class which allows to use dirty memory reading.
	/// </summary>
	/// <remarks>
	/// Something like C-lang union.
	/// Used only for performance purposes.
	/// </remarks>
	public static class ArrayConverter
	{
		[StructLayout(LayoutKind.Explicit)]
		private struct Union
		{
			[FieldOffset(0)] public byte[] Bytes;
			[FieldOffset(0)] public int[] Ints;
			[FieldOffset(0)] public UInt[] UInts;
			[FieldOffset(0)] public float[] Floats;
			[FieldOffset(0)] public double[] Doubles;
		}

		public static byte[] AsBytes(this double[] src)
		{
			return new Union { Doubles = src }.Bytes;
		}

		public static byte[] AsBytes(this float[] src)
		{
			return new Union { Floats = src }.Bytes;
		}

		public static UInt[] AsUInts(this int[] src)
		{
			return new Union { Ints = src }.UInts;
		}
	}
}