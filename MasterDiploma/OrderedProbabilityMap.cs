﻿using MasterDiplom.Tools;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MasterDiplom
{
	public sealed class OrderedProbabilityMap
		: IEnumerable<KeyValuePair<double, uint>>
	{
		private readonly SortedList<double, uint> _list;

		private static readonly IComparer<double> comparer = new DescComparer<double>();

		public int Count => _list.Count;
		public int Capacity => _list.Capacity;

		public uint this[double index]
		{
			get { return Get(index); }
			set { AddOrUpdate(index, value); }
		}

		public OrderedProbabilityMap(int capacity)
		{
			_list = new SortedList<double, uint>(capacity, comparer);
		}

		public OrderedProbabilityMap()
			: this(1 << 8)
		{
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
		public IEnumerator<KeyValuePair<double, uint>> GetEnumerator()
			=> _list.GetEnumerator();

		private void CreateIfNotExists(double key)
		{
			if (!_list.ContainsKey(key))
				_list.Add(key, 0);
		}

		private uint Get(double key)
		{
			CreateIfNotExists(key);
			return _list[key];
		}

		public void AddOrUpdate(double key, uint value)
		{
			CreateIfNotExists(key);
			_list[key] += value;
		}

		public void AddOrUpdate(double key) => AddOrUpdate(key, 1);

		public KeyValuePair<double, uint> GetAt(int index)
		{
			if (index >= _list.Count)
				return new KeyValuePair<double, uint>();
			return _list.ElementAt(index);
		}

		public static OrderedProbabilityMap operator *(OrderedProbabilityMap m1, OrderedProbabilityMap m2)
		{
			if (m1.IsNullOrEmpty())
				return m2;
			if (m2.IsNullOrEmpty())
				return m1;
			var result = new OrderedProbabilityMap(m1.Capacity);
			foreach (var e1 in m1)
				foreach (var e2 in m2)
					result.AddOrUpdate(e1.Key * e2.Key, e1.Value * e2.Value);
			return result;
		}
	}
}