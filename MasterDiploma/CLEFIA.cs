﻿namespace MasterDiplom
{
	using System;
	using System.Collections.Generic;
	using static Tools.Shorthands;
	using PartialSumStorage =
		System.Collections.Generic.Dictionary<int, System.Collections.Generic.Dictionary<uint, double>>;

#pragma warning disable S101

	public class CLEFIA : ParallelEstimator
	{
		protected override int BlocksCount { get { return 4; } }

		private readonly UInt[] S0;
		private readonly UInt[] S1;
		private readonly double MDP_S0;
		private readonly double MDP_S1;
		private double[] F0_seq;
		private double[] F1_seq;

		private double[][] _probabilities;
		protected override double[][] P
		{
			get
			{
				if (_probabilities == null)
				{
					_probabilities = new double[2][]
					{
						GetProbabilities(MDP(S0), 2 * _tplSize + 1),
						GetProbabilities(MDP(S1), 2 * _tplSize + 1),
					};
				}
				return _probabilities;
			}
		}

		private OrderedProbabilityMap _u_S0;
		protected OrderedProbabilityMap U_S0 
			=> _u_S0 ?? (_u_S0 = GetU(S0));

		private OrderedProbabilityMap _u_S1;
		protected OrderedProbabilityMap U_S1 
			=> _u_S1 ?? (_u_S1 = GetU(S1));

		private OrderedProbabilityMap[] _f0_UMap;
		private OrderedProbabilityMap[] F0_UMap 
			=> _f0_UMap ?? (_f0_UMap = GetMapForSequence(new[] { U_S0, U_S1, U_S0, U_S1 }));

		private OrderedProbabilityMap[] _f1_UMap;
		private OrderedProbabilityMap[] F1_UMap
			=> _f1_UMap ?? (_f1_UMap = GetMapForSequence(new[] { U_S1, U_S0, U_S1, U_S0 }));

		private PartialSumStorage F0_PartialSums { get; } = new PartialSumStorage();
		private PartialSumStorage F1_PartialSums { get; } = new PartialSumStorage();

		public CLEFIA(int tplSize = Default.TPL_SIZE)
			: base(tplSize)
		{
			_minRound = 2;
		}

		public CLEFIA(UInt[] s0, UInt[] s1, double mdpS0, double mdpS1, int tplSize = Default.TPL_SIZE)
			: this(tplSize)
		{
			S0 = s0;
			S1 = s1;
			MDP_S0 = mdpS0;
			MDP_S1 = mdpS1;
		}

		private void InitializeProbSequences()
		{
			F0_seq = new double[] { MDP_S0, MDP_S1, MDP_S0, MDP_S1 };
			F1_seq = new double[] { MDP_S1, MDP_S0, MDP_S1, MDP_S0 };
		}

		private void PrecalculatePartialSums()
		{
			var distinctCounts = new uint[] { 0, 1, 255, 64005, 16323825, 4162570275 };
			for (int i = 0; i < 1 << _tplSize; i++)
			{
				F0_PartialSums.Add(i, new Dictionary<uint, double>());
				F1_PartialSums.Add(i, new Dictionary<uint, double>());
				foreach (var w in distinctCounts)
				{
					CalculateAndStorePartialSum(F0_PartialSums, F0_UMap, w, i);
					CalculateAndStorePartialSum(F1_PartialSums, F1_UMap, w, i);
				}
			}
		}

		private OrderedProbabilityMap[] GetMapForSequence(OrderedProbabilityMap[] sequence)
		{
			var map = new OrderedProbabilityMap[1 << _tplSize];
			for (int v = 0; v < map.Length; v++)
			{
				var u = new OrderedProbabilityMap();
				for (int i = 0; i < _tplSize; i++)
					if ((v & (0x8 >> i)) > 0)
						u *= sequence[i];
				map[v] = u;
			}
			return map;
		}

		private double ChunkProbability(UInt x, double[] seq)
		{
			var p = 1d;
			if (x != 0)
				for (int i = 0; i < _tplSize; i++)
					if ((x & (1 << i)) > 0)
						p *= seq[i % seq.Length];
			return p;
		}

		private double CP0(UInt x) => ChunkProbability(x, F0_seq);
		private double CP1(UInt x) => ChunkProbability(x, F1_seq);

		private double[] GetProbabilities(double @base, int count)
		{
			var probabilities = new double[count];
			for (int i = 0; i < count; i++)
			{
				probabilities[i] = 1;
				for (int j = 0; j < i; j++)
					probabilities[i] *= @base;
			}
			return probabilities;
		}

		private OrderedProbabilityMap GetU(UInt[] s)
		{
			var size = 1 << 8;
			var u = new OrderedProbabilityMap(size);
			for (uint a = 1; a < size; a++)
			{
				var currentMax = 0;
				for (uint b = 0; b < size; b++)
				{
					int count = 0;
					for (uint x = 0; x < size; x++)
						if (s[x ^ a] == (s[x] ^ b))
							count++;
					if (count > currentMax)
						currentMax = count;
				}
				u.AddOrUpdate(currentMax / 256d);
			}
			return u;
		}

		private void CalculateAndStorePartialSum(
			PartialSumStorage storage,
			OrderedProbabilityMap[] map,
			uint w, int b)
		{
			if (storage[b].ContainsKey(w))
				return;
			var result = 0d;
			var currentMap = map[b];
			var w_tmp = w;
			var i = 0;
			while (w_tmp > 0)
			{
				var z = currentMap.GetAt(i);
				var count = z.Value > 0 ? Math.Min(w_tmp, z.Value) : w_tmp;
				result += count * z.Key;
				w_tmp -= count;
				i++;
			}
			storage[b].Add(w, result);
		}

		private double GetMaxOfRow(double[] row, Vector b, UInt e0, UInt e1)
		{
			var max = 0d;
			var Phi0 = Phi[b[0], e0];
			var Phi1 = Phi[b[2], e1];
			for (int i = 0; i < Phi0.Count; i++)
				for (int j = 0; j < Phi1.Count; j++)
				{
					var prob = row[b[3] << 12 | Phi0[i] << 8 | b[1] << 4 | Phi1[j]];
					if (max < prob)
						max = prob;
				}
			return max;
		}

		protected override double Primitive(Vector a, Vector b)
		{
			var result = 0d;
			var match
				 = TplMatches(b[0], b[3], a[2])
				&& TplMatches(b[1], a[2], a[3])
				&& TplMatches(b[2], b[1], a[0])
				&& TplMatches(b[3], a[0], a[1]);
			if (match)
				result = CP0(a[0]) * CP1(a[2]) * CP0(b[3]) * CP1(b[1]);
			return result;
		}

		protected override double Iterative(Vector a, Vector b)
		{
			var row = RowPool[a];
			var sum = 0d;
			for (var e0 = 0u; e0 < 1 << _tplSize; e0++)
			{
				for (var e1 = 0u; e1 < 1 << _tplSize; e1++)
				{
					sum += GetMaxOfRow(row, b, e0, e1)
						* F1_PartialSums[b[3]][W[b[3], e0]]
						* F0_PartialSums[b[1]][W[b[1], e1]];
				}
				if (sum > 1)
					break;
			}
			var result = sum;
			var bound = Bounds[b[3] << 12 | b[2] << 8 | b[1] << 4 | b[0]];
			if (result > bound)
				result = bound;
			return result;
		}

		public override double EstimateChunk(int rounds, int left, int right)
		{
			InitializeProbSequences();
			PrecalculatePartialSums();
			return base.EstimateChunk(rounds, left, right);
		}
	}

#pragma warning restore S101

}