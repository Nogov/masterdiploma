﻿namespace MasterDiplom
{
	internal interface IEstimationLogger
	{
		void Log(object obj);
	}
}