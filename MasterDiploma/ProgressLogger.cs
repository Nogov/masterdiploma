﻿using System;
using System.IO;
using System.Text;

namespace MasterDiplom
{
	internal class ProgressLogger : IEstimationLogger, IDisposable
	{
		private readonly Encoding _encoding = Encoding.UTF8;
		private readonly double _lower;
		private readonly double _upper;
		protected Stream Stream { get; private set; }
		public string MessageFormat { get; internal set; }

		private const string TIME_EVENT_FORMAT = "[{0}] {1}\n";

		public ProgressLogger(Stream stream, double lowerBound, double upperBound)
		{
			_lower = lowerBound;
			_upper = upperBound;
			Stream = stream;
		}

		public double GetProgressOf(double current)
		{
			if (current < _lower)
				return 0d;
			return (current - _lower) / (_upper - _lower);
		}

		public void LogProgressOf(double current)
		{
			var interim = GetProgressOf(current);
			Log($"{Math.Ceiling(interim * 100d)}%");
		}		

		public void Log(object obj)
		{
			var formatted = string.Format(TIME_EVENT_FORMAT, DateTime.Now.ToShortTimeString(), obj);
			var bytes = _encoding.GetBytes(formatted);
			Stream.Write(bytes, 0, bytes.Length);
		}

		#region IDisposable Support

		private bool disposedValue = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
					Stream.Dispose();
				disposedValue = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
		}

		#endregion
	}
}