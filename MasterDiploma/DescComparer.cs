﻿using System.Collections.Generic;

namespace MasterDiplom
{
	public class DescComparer<T> : IComparer<T>
	{
		#pragma warning disable S2234
		public int Compare(T x, T y) => Comparer<T>.Default.Compare(y, x);
		#pragma warning restore S2234
	}
}