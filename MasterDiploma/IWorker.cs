﻿namespace MasterDiplom
{
	public interface IWorker<out TResult, in TJob>
		where TJob : IJobDetails
	{
		TResult Do(TJob jobDetails);
	}
}
